#include <netdb.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#define MAXLEN 256
#define S_PORT "1234"

int main(int argc, char *argv[])
{
    int sfd;
    struct sockaddr cli;
    struct addrinfo hints, *res;
    char buf[MAXLEN];
    socklen_t clilen;
    int msglen;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET; // AF_INET6, AF_UNSPEC
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // socket za pasivni open, *.port
    getaddrinfo(NULL, S_PORT, &hints, &res);
    sfd = socket(res->ai_family,
                 res->ai_socktype,
                 res->ai_protocol);
    bind(sfd,
         res->ai_addr,
         res->ai_addrlen);
    while (1)
    {

        clilen = sizeof(cli);
        msglen = recvfrom(sfd, buf, MAXLEN, 0, &cli, &clilen);
        sendto(sfd, buf, msglen, 0, &cli, clilen);
    }
}